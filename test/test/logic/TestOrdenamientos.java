package test.logic;

import static org.junit.Assert.*;

import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.UBERTrip;

import org.junit.Before;
import org.junit.Test;

public class TestOrdenamientos {
	private MVCModelo modelo = new MVCModelo();
	private ArregloDinamico<UBERTrip> arreglo= new ArregloDinamico<UBERTrip>();
	private static int CAPACIDAD=1000;
	
	@Before
	public void setUp1() {
		
	}

	public void setUpAscendente() {
		for(int i =0; i< CAPACIDAD;i++){
			UBERTrip uber = new UBERTrip(i, i, i);
			arreglo.append(uber);
			
		}
	}

	public void setUpDescendente() {
		for(int i =CAPACIDAD-1; i>=0 ;i--){
			UBERTrip uber = new UBERTrip(i, i, i);
			arreglo.append(uber);
		}
	}

	public void setUpRandom() {
		for(int i =CAPACIDAD-1; i>0 ;i--){
			double numero = Math.random()*1000;
			UBERTrip uber = new UBERTrip(numero,numero,numero);
			arreglo.append(uber);
		}
	}
	
	
	@Test
	public void testShellSortAscendente()
	{
		setUpAscendente();
		arreglo = modelo.ordenarViajesShellSort(arreglo);
		for(int i=0;i<CAPACIDAD;i++)
		{
			UBERTrip uber = new UBERTrip(i, i, i);
			assertEquals(0, uber.compareTo(arreglo.get(i)));
		}
	}
	@Test
	public void testShellSortDescendentes()
	{
		setUpDescendente();
		arreglo =modelo.ordenarViajesShellSort(arreglo);
		for(int i=0;i<CAPACIDAD;i++)
		{
			UBERTrip uber = new UBERTrip(i, i, i);
			
		assertEquals(0, uber.compareTo(arreglo.get(i)));
		}
	}
	@Test
	public void testShellSortRandom()
	{
		setUpRandom();
		arreglo =modelo.ordenarViajesShellSort(arreglo);
		for(int i=0;i<CAPACIDAD-2;i++)
		{
			UBERTrip uber = arreglo.get(i);
			UBERTrip uber2 = arreglo.get(i+1);
			assertEquals(-1, uber.compareTo(uber2));
		}
	}

	@Test
	public void testMergeSortAscendente()
	{
		setUpAscendente();
		arreglo = modelo.ordenarViajesShellSort(arreglo);
		for(int i=0;i<CAPACIDAD;i++)
		{
			UBERTrip uber = new UBERTrip(i, i, i);
			assertEquals(0, uber.compareTo(arreglo.get(i)));
		}
	}
	@Test
	public void testMergeSortDescendentes()
	{
		setUpDescendente();
		arreglo = modelo.ordenarViajesShellSort(arreglo);
		for(int i=0;i<CAPACIDAD;i++)
		{
			UBERTrip uber = new UBERTrip(i, i, i);
			
		assertEquals(0, uber.compareTo(arreglo.get(i)));
		}
	}
	@Test
	public void testMergeSortRandom()
	{
		setUpRandom();
		arreglo = modelo.ordenarViajesShellSort(arreglo);
		for(int i=0;i<CAPACIDAD-2;i++)
		{
			UBERTrip uber = arreglo.get(i);
			UBERTrip uber2 = arreglo.get(i+1);
			assertEquals(-1, uber.compareTo(uber2));
		}
	}
	
	@Test
	public void testQuickSortAscendente()
	{
		setUpAscendente();
		arreglo =modelo.ordenarViajesQuickSort(arreglo);
		for(int i=0;i<CAPACIDAD;i++)
		{
			UBERTrip uber = new UBERTrip(i, i, i);
			assertEquals(0, uber.compareTo(arreglo.get(i)));
		}
	}
	@Test
	public void testQuickSortDescendentes()
	{
		setUpDescendente();
		arreglo =modelo.ordenarViajesQuickSort(arreglo);
		for(int i=0;i<CAPACIDAD;i++)
		{
			UBERTrip uber = new UBERTrip(i, i, i);
			
		assertEquals(0, uber.compareTo(arreglo.get(i)));
		}
	}
	@Test
	public void testQuickSortRandom()
	{
		setUpRandom();
		arreglo =modelo.ordenarViajesQuickSort(arreglo);
		for(int i=0;i<CAPACIDAD-2;i++)
		{
			UBERTrip uber = arreglo.get(i);
			UBERTrip uber2 = arreglo.get(i+1);
			assertEquals(-1, uber.compareTo(uber2));
		}
	}
}
