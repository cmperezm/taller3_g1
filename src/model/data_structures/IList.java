package model.data_structures;

public interface IList<E>  {

	public void addFirst(E item);
	public void removeFirst();
	public void append(E item);
	public void remove(int pos);
	public E get (int pos);
	public int size();
	public boolean isEmpty();
	public void replace(E item, int pos);
	
	
}
