package model.logic;

public class UBERTrip implements Comparable<UBERTrip>{

	private double hora;

	private double mean_travel_time;

	private double standard_deviation_travel_time;
	public UBERTrip(double hora, double mean_travel_time,double standard_deviation_travel_time)
	{
		this.hora=hora;
		this.mean_travel_time=mean_travel_time;
		this.standard_deviation_travel_time=standard_deviation_travel_time;
	}
	@Override
	public int compareTo(UBERTrip o) {
		// TODO Auto-generated method stub

		if(this.mean_travel_time > o.mean_travel_time){
			return 1;
		} else if(this.mean_travel_time < o.mean_travel_time){
			return -1;
		} else if(this.standard_deviation_travel_time > o.standard_deviation_travel_time){
			return 1;
		} else if(this.standard_deviation_travel_time < o.standard_deviation_travel_time){
			return -1;
		}
		return 0;

	}
	public double getHora() {
		return hora;
	}

	public double getMean() {
		return mean_travel_time;
	}public double getStandard() {
		return standard_deviation_travel_time;

	}
}