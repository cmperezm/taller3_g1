package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Random;

import com.opencsv.CSVReader;


import model.data_structures.ArregloDinamico;


/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	ArregloDinamico<UBERTrip> datos = new ArregloDinamico<UBERTrip>();

	public void cargar(int cuatrimestre) {
		String line = null;
		int sizeMonth = 0;
		int sizehour = 0;
		int sizeweek = 0;
		double menor=9999999;
		double mayor = 0;
		File fileHour = new File("./data/bogota-cadastral-2018-" + cuatrimestre + "-All-HourlyAggregate.csv");
		try (BufferedReader br = new BufferedReader(new FileReader(fileHour))) {

			br.readLine();
			line = br.readLine();

			while (sizehour<104857) {

				String[] data = line.split(",");
				
				double dato3 = Double.parseDouble(data[2]);
				double dato4 = Double.parseDouble(data[3]);
				double dato5 = Double.parseDouble(data[4]);
			
				UBERTrip viaje = new UBERTrip(dato3,dato4,dato5);
				datos.append(viaje);
				sizehour++;
				line= br.readLine();
				
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Total de viajes en el archivo hora del trimestre "+cuatrimestre+" = "+sizehour+"");
		System.out.println("La zona con menor identificador en todos los archivos del trimestre seleccionado: "+menor+"");
		System.out.println("La zona con mayor identificador en todos los archivos del trimestre seleccionado: "+mayor+"");
		
		
			
		}

	// public UBERTrip[]<T implements Comparable> consultarViajesEnHora(double
	// hora){
	public ArregloDinamico<UBERTrip> consultarViajesEnHora(double hora){

		ArregloDinamico<UBERTrip> viajes = new ArregloDinamico<>();

		for (int i = 0; i < datos.size(); i++) {

			UBERTrip viajeActual = datos.get(i);

			if(viajeActual.getHora() == hora){
				viajes.append(viajeActual);
			}

		}
		
		System.out.println("Numero de viajes: " + viajes.size());

		return viajes;
	}

	private static boolean less(UBERTrip v,UBERTrip w){
		return v.compareTo(w) < 0;
	}

	private static void exch(ArregloDinamico<UBERTrip> array, int i, int j){
		UBERTrip swap = array.get(i);
		array.replace(array.get(j), i);
		array.replace(swap, j);
	}

	public ArregloDinamico<UBERTrip> ordenarViajesShellSort(ArregloDinamico<UBERTrip> viajesAOrdenar) {
		
//		System.out.println("Empieza ordenamiento ShellSort");
		
		//Empezar a medir el tiempo
		long startTimeShellSort = System.currentTimeMillis(); 

		int N = viajesAOrdenar.size();
		int h = 1;

		while(h < N/3) {
			h = 3*h +1 ;
		}

		while(h >= 1){
//			System.out.println("Valor de h actual:" + h);
			for (int i = h; i < N; i++) {
				for (int j = i; j >= h && less(viajesAOrdenar.get(j), viajesAOrdenar.get(j-h)); j -= h) {
					exch(viajesAOrdenar, j, j-h);
				}
			}
			h = h/3;
		}

		//Terminar de medir el tiempo
		long endTimeShellSort = System.currentTimeMillis(); 

		System.out.println("Tiempo:" + (startTimeShellSort - endTimeShellSort));
		System.out.println("Primeros 10 viajes:");
		System.out.println("Últimos 10 viajes:");
		
		System.out.println("Termina ordenamiento ShellSort");
		
		return viajesAOrdenar;

	}

	private static void merge(ArregloDinamico<UBERTrip> array, ArregloDinamico<UBERTrip> aux, int lo, int mid, int hi ){
		int i = lo;
		int j = mid + 1;

		for(int k = lo; k <= hi; k++)
			aux.replace(array.get(k), k);

		for (int k = lo; k <= hi; k++) {
			// Juntar a array[lo..hi]
			if(i > mid) array.replace(aux.get(j++), k);
			if(i > mid) array.replace(aux.get(j++), k);
			else if(j > hi) array.replace(aux.get(i++), k);
			else if(less(aux.get(j), aux.get(i))) array.replace(aux.get(j++), k);
			else array.replace(aux.get(i++), k);
		}
	}

	private static void mergeSort(ArregloDinamico<UBERTrip> array, ArregloDinamico<UBERTrip> aux, int lo, int hi){

		int mid = lo + (hi - lo)/2;

		//ordenar mitad izquierda
		mergeSort(array, aux, lo, mid);

		//ordenar mitad derecha
		mergeSort(array, aux, mid + 1, hi);

		merge(array, aux, lo, mid, hi);
	}

	public ArregloDinamico<UBERTrip> ordenarViajesMergeSort(ArregloDinamico<UBERTrip> viajesAOrdenar) {
		
		System.out.println("Empieza ordenamiento MergeSort");
		
		//Empezar a medir el tiempo
		long startTimeShellSort = System.currentTimeMillis();

		ArregloDinamico<UBERTrip> aux = new ArregloDinamico<>();

		mergeSort(viajesAOrdenar, aux, 0, viajesAOrdenar.size() -1);

		//Terminar de medir el tiempo
		long endTimeShellSort = System.currentTimeMillis(); 

		System.out.println("Tiempo:" + (startTimeShellSort - endTimeShellSort));
		System.out.println("Primeros 10 viajes:");
		System.out.println("Últimos 10 viajes:");
		
		System.out.println("Termina ordenamiento MergeSort");

		return viajesAOrdenar;

	}

	private static int partition(ArregloDinamico<UBERTrip> array, int lo, int hi){

		//romper arreglo a [lo..i-1] (izquierda), [i] (pivote), [i+1...hi] (derecha)

		//indice izquierdo
		int i = lo;

		//indice derecho
		int j = hi + 1;

		//pivote
		UBERTrip v = array.get(lo);

		while(true){
			//Recorrer derecha

			while(less(array.get(++i), v)) if(i == hi) break;

			//Recorrer izquierda

			while(less(v, array.get(--j))) if(j == lo) break;

			//Verificar si el recorrido esta completo

			if(i >= j) break;

			//Intercambiar

			exch(array, i, j);
		}

		exch(array, lo, j);
		return j;
	}

	private static void quickSort(ArregloDinamico<UBERTrip> array, int lo, int hi){
		int j = partition(array, lo, hi);

		//Ordenar izquierda
		quickSort(array, lo, j-1);

		//Ordenar derecha
		quickSort(array, j+1, hi);
	}

	public ArregloDinamico<UBERTrip> ordenarViajesQuickSort(ArregloDinamico<UBERTrip> viajesAOrdenar) {
		
		System.out.println("Empieza ordenamiento QuickSort");
		
		//Empezar a medir el tiempo
		long startTimeShellSort = System.currentTimeMillis();


		//StdRandom.shuffle(viajesAOrdenar);
		quickSort(viajesAOrdenar, 0, viajesAOrdenar.size() - 1);


		//Terminar de medir el tiempo
		long endTimeShellSort = System.currentTimeMillis(); 

		System.out.println("Tiempo:" + (startTimeShellSort - endTimeShellSort));

		System.out.println("Primeros 10 viajes:");
		System.out.println("Últimos 10 viajes:");
		
		System.out.println("Termina ordenamiento QuickSort");

		return viajesAOrdenar;

	}

	public void CompararAlgoritmosOrdenamiento() {

		// ShellSort

		// medición tiempo actual
		long startTimeShellSort = System.currentTimeMillis(); 
		this.ordenarViajesShellSort(datos);
		long endTimeShellSort = System.currentTimeMillis(); 

		// duracion de ejecucion del algoritmo
		long durationShellSort = endTimeShellSort - startTimeShellSort; 

		System.out.println("Tiempo de ordenamiento ShellSort: " + durationShellSort+ " milisegundos");

		// MergeSort

		// medición tiempo actual
		long startTimeMergeSort = System.currentTimeMillis(); 	
		this.ordenarViajesMergeSort(datos);
		long endTimeMergeSort = System.currentTimeMillis(); 

		// duracion de ejecucion del algoritmo
		long durationMergeSort = endTimeMergeSort - startTimeMergeSort; 

		System.out.println("Tiempo de ordenamiento MergeSort: " + durationMergeSort+ " milisegundos");

		// QuickSort

		// medición tiempo actual
		long startTimeQuickSort = System.currentTimeMillis(); 
		this.ordenarViajesQuickSort(datos);
		long endTimeQuickSort = System.currentTimeMillis(); 

		// duracion de ejecucion del algoritmo
		long durationQuickSort = endTimeQuickSort - startTimeQuickSort; 

		System.out.println("Tiempo de ordenamiento QuickSort: " + durationQuickSort+ " milisegundos");
	}

}
